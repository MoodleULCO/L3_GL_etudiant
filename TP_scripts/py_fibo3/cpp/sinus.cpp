#include <cmath>
#include <pybind11/pybind11.h>

double f(double a, double b, double x){
	return sin(2.0*M_PI*(a*x + b));
}

PYBIND11_PLUGIN(sinus){
	pybind11::module m("sinus");
	m.def("f",&f);
	return m.ptr();
}
