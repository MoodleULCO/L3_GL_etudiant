
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import fibo
import sinus

xs = range(10)
ys = [fibo.fiboIterative(x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('plot_fibo.png')
plt.clf()

az = 2
bz = 0.25

import numpy as np

xz = np.linspace(0,1,100)

fz = [sinus.f(az,bz,z) for z in xz]

plt.plot(xz,fz)
plt.xlabel('x')
plt.ylabel('sinus x')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()
